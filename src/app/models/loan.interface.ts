export interface Loan {
    creditStatus: boolean;
    loanValue: number;
    paymentStatus: boolean;
    userId: string;
    id?: string;
}
