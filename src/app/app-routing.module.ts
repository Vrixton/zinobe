import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'users',
        loadChildren: () =>
          import('./pages/users/users.module').then((m) => m.UsersModule),
      },
      {
        path: 'users/:id',
        loadChildren: () =>
          import('./pages/users/user/user.module').then((m) => m.UserModule),
      },
      {
        path: 'loans',
        loadChildren: () =>
          import('./pages/loans/loans.module').then((m) => m.LoansModule),
      },
      {
        path: 'apply-for-loan',
        loadChildren: () =>
          import('./pages/apply-for-loans/apply-for-loans.module').then(
            (m) => m.ApplyForLoansModule
          ),
      },
    ],
  },

  { path: '**', redirectTo: 'loans' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
