import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Capital } from '../../models/capital.interface';
import { environment } from '../../../environments/environment'
@Injectable({
  providedIn: 'root',
})
export class CapitalService {
  capitalId: string = environment.capitalId;
  capitalEnv: number = environment.capitalBank;
  capitalInfo: any;

  constructor(private afs: AngularFirestore) { }

  public getCapital(): Observable<Capital[]> {
    return this.afs
      .collection('capital')
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as Capital;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }


  public getCapitalInfo(): Observable<Capital[]> {
    return this.afs.doc<Capital[]>(`capital/${this.capitalId}`).valueChanges();
  }

  public updateCapital(value, option) {
    const getC = this.getCapital().subscribe(capitalInfo => {
      const newCapital = {
        capital: capitalInfo[0].capital,
        loans: (option === 1) ? (capitalInfo[0].loans + value) : (capitalInfo[0].loans),
        payments: (option === 1) ? (capitalInfo[0].payments) : (capitalInfo[0].payments + value),
        total: (option === 1) ? (capitalInfo[0].total - value) : (capitalInfo[0].total + value),
      }
      this.afs.collection('capital').doc(this.capitalId).update(newCapital);
      destroyGetC();
    })
    function destroyGetC() {
      getC.unsubscribe();
    }
  }

  public restoreCapital() {
    return this.afs.collection('capital').doc(this.capitalId).update({
      capital: this.capitalEnv,
      loans: 0,
      payments: 0,
      total: this.capitalEnv,
    });
  }
}
