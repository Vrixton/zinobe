import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-apply-loan',
  templateUrl: './apply-loan.component.html',
  styleUrls: ['./apply-loan.component.scss']
})
export class ApplyLoanComponent implements OnInit {
  capitalBank: number = environment.capitalBank;
  minLoanValue: number = environment.minLoanValue;
  amountFunctions: number = environment.amountFunctions;
  loanFormGroup: FormGroup;
  @Output() loanValueSelected = new EventEmitter<string>();

  constructor(
    private _formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.loanFormGroup = this._formBuilder.group({
      loanValue: [environment.loanValue, Validators.required],
    });
  }

  add() {
    this.loanFormGroup = this._formBuilder.group({
      loanValue: [
        this.loanFormGroup.value.loanValue + this.amountFunctions,
        Validators.required,
      ],
    });
  }

  substract() {
    this.loanFormGroup = this._formBuilder.group({
      loanValue: [
        this.loanFormGroup.value.loanValue - this.amountFunctions,
        Validators.required,
      ],
    });
  }

  applyForLoan(data) {
    this.loanValueSelected.emit(data.value.loanValue);
  }
}
