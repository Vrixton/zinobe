import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Capital } from '../../models/capital.interface';
import { Observable } from 'rxjs';
import { CapitalService } from '../../services/capital/capital.service';
@Component({
  selector: 'app-capital',
  templateUrl: './capital.component.html',
  styleUrls: ['./capital.component.scss']
})
export class CapitalComponent implements OnInit {
  capitalCard: boolean = false;
  public capital$: Observable<Capital[]>;
  constructor(private _capital: CapitalService) { }

  ngOnInit(): void {
    this.capital$ = this._capital.getCapital();
  }

  restoreCapital() {
    this._capital.restoreCapital();
  }

}
