import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ModalRoutingModule } from './modal-routing.module';
import { ModalComponent } from './modal.component';
import { ApplyLoanModule } from '../../utils/apply-loan/apply-loan.module';

@NgModule({
  declarations: [
    ModalComponent
  ],
  imports: [
    CommonModule,
    ModalRoutingModule,
    ApplyLoanModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [
    ModalComponent,
  ]
})
export class ModalModule { }