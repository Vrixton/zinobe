import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyForLoansComponent } from './apply-for-loans.component';

describe('ApplyForLoansComponent', () => {
  let component: ApplyForLoansComponent;
  let fixture: ComponentFixture<ApplyForLoansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplyForLoansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyForLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
