import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { LoanService } from '../../services/loan/loan.service';
import { TranslateService } from "@ngx-translate/core";
import { NotificationService } from '../../services/notification/notification.service';
@Component({
  selector: 'app-apply-for-loans',
  templateUrl: './apply-for-loans.component.html',
  styleUrls: ['./apply-for-loans.component.scss'],
})
export class ApplyForLoansComponent implements OnInit {
  capitalBank: number = environment.capitalBank;
  minLoanValue: number = environment.minLoanValue;
  amountFunctions: number = environment.amountFunctions;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isEditable = false;
  constructor(
    private _formBuilder: FormBuilder,
    private translate: TranslateService,
    private userService: UserService,
    private loanService: LoanService,
    private _notification: NotificationService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      loanValue: [environment.loanValue, Validators.required],
    });
  }

  selectLoan(value, stepper) {
    this.secondFormGroup = this._formBuilder.group({
      loanValue: [
        value,
        Validators.required,
      ],
    });
    this.saveUser(this.firstFormGroup, this.secondFormGroup, stepper);
  }

  saveUser(userData, loanData, stepper) {
    loanData.value.creditStatus = this.randomBoolean();
    loanData.value.paymentStatus = false;
    userData.value.lastCreditApproved = loanData.value.creditStatus;
    this.userService.saveUser(userData.value).then((userRes) => {
      loanData.value.userId = userRes.id;
      this.translate.get("USER_CREATED_SUCESSFULLY").subscribe((res: string) => {
        this._notification.showSuccessMessage(res);
        this.loanService.saveLoan(loanData.value).then((loanRes) => {
          stepper.next();
        });
      });
    });
  }

  randomBoolean() {
    return Boolean(Math.round(Math.random()));
  }

}
