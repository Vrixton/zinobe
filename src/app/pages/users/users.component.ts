import { Component, AfterViewInit } from '@angular/core';
import { User } from '../../models/user.interface';
import { UserService } from '../../services/user/user.service';
import { Observable } from 'rxjs';
import { TranslateService } from "@ngx-translate/core";
import { NotificationService } from '../../services/notification/notification.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements AfterViewInit {
  displayedColumns: string[] = ['name', 'email', 'actions', 'options'];
  public users$: Observable<User[]>;

  ngAfterViewInit() { }

  constructor(private userService: UserService,
    private _notification: NotificationService,
    private translate: TranslateService) {
    this.getAllUsers();
  }

  getAllUsers() {
    this.users$ = this.userService.getAllUsers();
  }
}
