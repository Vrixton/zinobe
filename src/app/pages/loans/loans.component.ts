import { Component, AfterViewInit } from '@angular/core';
import { Loan } from '../../models/loan.interface';
import { LoanService } from '../../services/loan/loan.service';
import { Observable } from 'rxjs';
import { TranslateService } from "@ngx-translate/core";
import { NotificationService } from '../../services/notification/notification.service';

@Component({
  selector: 'app-loans',
  templateUrl: './loans.component.html',
  styleUrls: ['./loans.component.scss']
})
export class LoansComponent implements AfterViewInit {

  displayedColumns: string[] = ['loanValue', 'paymentStatus', 'paymentDate', 'actions'];
  public loans$: Observable<Loan[]>;

  ngAfterViewInit() { }

  constructor(private loanService: LoanService,
    private _notification: NotificationService,
    private translate: TranslateService) {
    this.getAllLoans();
  }

  getAllLoans() {
    this.loans$ = this.loanService.getAllLoans();
  }

  payLoan(data) {
    data.paymentStatus = true;
    data.paymentDate = new Date();
    this.loanService.updateLoan(data).then((user) => {
      this.translate.get("SUCESSFUL_PAYMENT").subscribe((res: string) => {
        this._notification.showSuccessMessage(res);
      });
    })
  }

}
