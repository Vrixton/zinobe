export class Constants {
  public static SIDENAV_OPTIONS = [
    {
      icon: 'people',
      name: 'USERS',
      url: '/users',
    },
    {
      icon: 'monetization_on',
      name: 'LOANS',
      url: '/loans',
    }
  ];
}
